// This is Todo.js

import React, { Component } from 'react';
import '../App.css';

export default class TodoList extends Component {

    onhandleRemove = ( index ) => {
        this.props.todo.splice(index, 1)
        this.setState({todos :this.props.todo})
    }


    render() {
        return (
            <div>
                <ul className = 'list-group'>
                    {
                        this.props.todo.map(
                            ( todos, index ) => <li className = 'list-group-item' key = { index } >
                            { todos }
                            &nbsp;&nbsp;&nbsp;
                            <button type="button" className = "btn btn-outline-success" onClick = { this.onhandleRemove }> Completed </button>
                        </li>
                        )
                    }
                </ul>
            </div>
        )
    }
}
