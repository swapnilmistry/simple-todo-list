// This is App.js

import React, { Component } from 'react';
import TodoList from './compoents/TodoList';
import './App.css';


export default class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      item_name: '',
      todos: []
    }
  }

  handleonChange = ( event ) => {
    this.setState({
      item_name: event.target.value,
    })
  }

  handleonSubmit = ( event ) => {
    //let todolist = this.state.todos;
    //todolist.push(this.state.item_name);
    event.preventDefault();
    this.setState({
      item_name: '',
      //todos: [...this.state.todos, todolist ],
      todos: this.state.todos.concat([this.state.item_name])
    })
    
    //console.log(this.state.todos)
  }

  render() {
    return (
      <div className = 'App'>
        <form className = "form-inline my-2 my-lg-0" onSubmit = { this.handleonSubmit }> 
          <div className = 'form-group'>
          <input className = 'form-control mr-sm-2'
          value = { this.state.item_name }
          placeholder = 'Enter Todos' 
          onChange = { this.handleonChange } required/>
            <button className = 'btn btn-primary'> Add To Todo's </button>
          </div>
        </form>
        <TodoList todo = { this.state.todos } />
      </div>
    )
  }
}
